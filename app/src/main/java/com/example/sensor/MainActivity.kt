package com.example.sensor

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.sensor.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val listSensor: List<Sensor> = sensorManager.getSensorList(Sensor.TYPE_ALL)

        for (i in 1 until listSensor.size) {
            binding.textView.visibility
            binding.textView.append(
                """
                ${listSensor[i].name}
                ${listSensor[i].vendor}
                ${listSensor[i].version}
                """.trimIndent()
            )
        }
    }
}